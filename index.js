let posts = [];
let count = 1;

// Add new post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	// 'preventDefault' function stops the default behavior of forms when submitting them which is refreshing the page.
	e.preventDefault();

	// Pushes a new post object into the 'posts' array
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector('#txt-body').value
	});

	// Responsible for displaying each post in the HTML document
	count++;

	console.log(posts);
	showPost(posts);
	alert('Successfully Added');
})

// For showing posts in the div element
const showPost = (posts) => {
	let postEntries = '';

    // Loops through entire 'posts' array and sets their properties (id, title, body) to their respective HTML elements
	posts.forEach((post) => {

		postEntries += `
			<div id='post-${post.id}'>
				<h3 id='post-title-${post.id}'>${post.title}</h3>
				<p id='post-body-${post.id}'>${post.body}</p>

				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;

	})

    // By using 'innerHTML' we are able to convert the string format of 'post_entries' variable into HTML format
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Posts
// Transfers the id, title, and body to the input field of the edit post form
const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

// Update Posts
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	// Loops through the entire 'posts' array to find a matching post with the same ID in the form
	for(let i=0; i<posts.length; i++) {

        // If a post has been found with the same ID in the form, then update that post
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {

            // Re-assigns the current title and body of the matching post to the new values from the edit form
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPost(posts);
			alert("Successfully Updated!");

			break;
		}
	}
})

const deletePost = function(postId){

    if(posts.length >= 2) {
        posts.splice(postId-1,1)
    }else {
        posts.splice(0,1)
    }

    document.getElementById(`post-${postId}`).remove()
    console.log(posts)
}
